import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait

options = Options()
options.add_argument('--headless')
options.add_argument('--log-level=3')
options.add_experimental_option('w3c', False)
caps = DesiredCapabilities.CHROME
caps['loggingPrefs'] = { 'performance': 'ALL', 'browser': 'ALL' }
browser = webdriver.Chrome(options=options, desired_capabilities=caps)

browser.get('https://www.youtube.com/live_chat?continuation=0ofMyANoGlBDamdLRFFvTFJuQklUVWR4TVZwWFJrMHFKd29ZVlVNdGFFMDJXVXAxVGxsV1FXMVZWM2hsU1hJNVJtVkJFZ3RHY0VoTlIzRXhXbGRHVFNBQjABaASCAQIIBIgBAaABrcnu1buo5AI%253D')

old_comments_id = set()

while True:
    comments_node = browser.find_elements_by_css_selector(
        'div.yt-live-chat-item-list-renderer#items yt-live-chat-text-message-renderer,yt-live-chat-paid-message-renderer,yt-live-chat-legacy-paid-message-renderer'
    )
    new_comments_dict = {comment.id: {
        'author': comment.find_element_by_css_selector('#author-name').text,
        'message': comment.find_element_by_css_selector('#message').text,
        'amount': comment.find_element_by_css_selector('#purchase-amount').text if len(comment.find_elements_by_css_selector('#purchase-amount')) != 0 else 0,
    } for comment in comments_node}

    diff = new_comments_dict.keys() - old_comments_id
    old_comments_id = set(sorted(list(old_comments_id))[-250:]) | diff
    new_comments = [new_comments_dict[id] for id in diff]
    print(len(old_comments_id), new_comments, '\n')
    time.sleep(1)

browser.quit()
